# UpSkillLab

**Training of EPAM Systems**

_Content:_
1. Git
2. Maven
 (+ multi-module maven)
3. JUnit
4. Logging (Log4j2)
5. XML and JSON